/*
 * Count Amicable Numbers
 */

#include <iostream>
#include <vector>

using namespace std;

int CalcDivisorSum(int num);
int CalcAmicableSum(vector<int> & divisor_sums);

int END_RANGE = 10000;

int main(){
  vector<int> divisor_sums (END_RANGE+1);

  for(int i =1; i <= END_RANGE; i++){
    int divisor_sum = CalcDivisorSum(i);
    divisor_sums[i] = divisor_sum;
  }

  int amicable_sum = CalcAmicableSum(divisor_sums);
  cout << "Sum of Amicable Numbers is " << amicable_sum << endl;
}

/*
 * Loop over Vector to Check For Amicable Pairs,
 * then sum them together.
 */
int CalcAmicableSum(vector<int> & divisor_sums){
  int amicable_sum =0;

  for(int index = 1; index < divisor_sums.size(); index++){

    // Don't Count Itself
    if( index == divisor_sums[index]) continue;
    
    // Make Sure Sum is within Bounds
    if(divisor_sums[index] >= divisor_sums.size()) continue;

    //Check For Pair
    if(index == divisor_sums[ divisor_sums[index] ]){
    
      amicable_sum += index;
    }
  }
  return amicable_sum;
}

/*
 * Given a Number, Calculate the Sum of it's divisors
 */
int CalcDivisorSum(int num){
  int sum = 0;
  for(int i = 1; i < num; i++){

    if( (num % i) == 0){
      sum += i;
    }
  }
  return sum;
}

