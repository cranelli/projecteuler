/*
 * Short function to test if two strings are anagrams.
 */

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

bool IsAnagram(string str1, string str2);
void RemoveWhiteSpace(string & str);

int main(){
  string str1, str2;
  cout << "Input 2 strings" << endl;
  getline(cin, str1);
  getline(cin, str2);
  cout << IsAnagram(str1, str2) << endl;

}

/*
 * Store the number of letters in each string.
 * If contents of maps match, return true.
 * By prechecking strings are same length, possible O(1)
 * solution, and only have to iterate over the contents of 
 * one map.
 */
bool IsAnagram(string str1, string str2){
  RemoveWhiteSpace(str1);
  RemoveWhiteSpace(str2);
  //Check if same length
  if( str1.length() != str2.length() ) return false;
  
  //Sort and Compare Strings
  sort(str1.begin(), str1.end());
  sort(str2.begin(), str2.end());

  if( str1 == str2 ) return true;
  
  return false;
}

void RemoveWhiteSpace(string & str){
  for(string::iterator it = str.begin(); it != str.end(); it++){
    if( isspace(*it) ) str.erase(it);
  }
}

