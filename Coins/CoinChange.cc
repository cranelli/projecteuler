#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;


long ChangeCombinations(int total, vector<int> & coins);
long ChangeCombinations(int total, int last_coin, vector<int> & coins);


map<int, long> STORE;

long ChangeCombinations(int total, vector<int> & coins){
    
    if(total == 0) return 0;
    
    int last_coin = coins[coins.size()-1];
    
    return ChangeCombinations(total, last_coin, coins);
    
}

long ChangeCombinations(int total, int last_coin, vector<int> & coins){
    
    //cout << total << " ";
    if(total == 0) return 1;
    if(total < 0) return 0;
    
    int lookup = total *50 + last_coin -1;
    map<int, long>::iterator it = STORE.find(lookup);
    
    if(it == STORE.end()){
        long combinations = 0;
        
        for(int i = coins.size()-1; i >= 0; i--){
            if(coins[i] > last_coin) continue;
            //cout << coins[i] << "c" << endl;
            combinations += ChangeCombinations(total - coins[i], coins[i], coins);
        }
        STORE[lookup] = combinations;
    }
    
    return STORE[lookup];
    
}



int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    
    int N, M;
    
    cin >> N;
    cin >> M;
    
    vector<int> coins(M);
    for(int i=0; i < M; i++){
        cin >> coins[i];
    }
    
    sort(coins.begin(), coins.end());
    
    cout << ChangeCombinations(N, coins) << endl;
    
    return 0;
}
