/* 
 * Counting the number of unique piles
 * n coins can be split between
 */

#include <iostream>
#include <iomanip>
#include <map>

using namespace std;

map <pair<int,int>, int > STORE_COMBINATIONS;


int CountPileCombinations(int n_coins, int max_per_pile);

int main(){
  for(int n_coins =0; n_coins < 1000; n_coins++){
    int max_per_pile = n_coins;
    int combinations = CountPileCombinations(n_coins, max_per_pile);

    if(combinations % 1000 == 0)  cout << n_coins << " " << combinations << endl;
  }
}

/*
 * Recursive method to count the possilbe number of combinations
 * given n_coins and max_per_pile.
 */
int CountPileCombinations(int n_coins, int max_per_pile){
  pair<int, int> key (n_coins, max_per_pile);

  if( STORE_COMBINATIONS.count(key) > 0){
    return STORE_COMBINATIONS[key];
      }

  if(n_coins == 0) {
    return 1;
  } 
 
  if(n_coins < 0) return 0;
  
  int combinations=0;
  for(int i =1; i <= max_per_pile; i++){
    combinations += CountPileCombinations(n_coins-i, min(i, max_per_pile));
  }
  
  
  //cout << endl;
  
  STORE_COMBINATIONS.insert(pair< pair <int, int>, int > (key, combinations));
  
  return combinations;
  
}
