#!/bin/bash

count=0;
previous=""

while read line;
do
    echo $line $previous
    if [ "$line" = "$previous" ]; then
	count=$(($count+1))
    fi

    previous=$line

done < data.dat


echo $count
