#!/bin/bash

DIR1="dir1/"
DIR2="dir2/"

# Files only in DIR1
#diff -r $DIR1 $DIR2  | grep $DIR1 | grep "\."| sed 's/.*://'


ls -R $DIR1 | grep "\."  | sort -n > list1.txt
ls -R $DIR2 | grep "\."  | sort -n > list2.txt


echo "Files Only Contained in " $DIR1
diff  list1.txt list2.txt | grep '<' | sed 's/.*< //'

echo "Files Only Contained in " $DIR2
diff  list1.txt list2.txt | grep '>' | sed 's/.*> //'
