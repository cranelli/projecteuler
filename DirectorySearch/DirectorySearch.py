import sys
import os



def CompareDirs(dir1, dir2):
    list1 = MakeList(dir1)
    list2 = MakeList(dir2)

    print "Files in ", dir1, " not contained in ", dir2, ":"
    notList1 = CompareLists(list1, list2)
    print notList1

    print
    print "Files in ", dir2, " not contained in ", dir1, ":"
    notList2 = CompareLists(list2, list1)
    print notList2
    

def MakeList(dir):
    list = []
    for root, dirs, files in os.walk(dir):
        list.extend(files)     
    return list


def CompareLists(list1, list2):
    notList = []
    for file in list1:
        if list2.count(file) == 0:
            notList.append(file)

    return notList
        

if __name__=="__main__":
    CompareDirs(sys.argv[1], sys.argv[2])
