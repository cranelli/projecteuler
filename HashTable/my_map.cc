/*
 * MyMap Class
 * Using HashTable
 */

#include "my_map.h"
#include <iostream>
//#include <string>

using namespace std;

/*
 * Public Functions
 */

//Constructur
template <typename ValueType>
MyMap<ValueType>::MyMap(){
  for(int i =0; i < nBuckets; i++){
    buckets[i] = NULL;
  }
}

//Destructor
template <typename ValueType>
MyMap<ValueType>::~MyMap(){
  for(int i =0; i < nBuckets; i++){
    deleteBucketChain(buckets[i]);
  }
}

//Add
template <typename ValueType>
void MyMap<ValueType>::Add(string key, ValueType value){
  int bucket = hash(key);
  cellT *cell = FindCell(buckets[bucket], key);

  if(cell == NULL) {
    cell = new cellT;
    cell->key = key;
    cell->link = buckets[bucket];
    
    buckets[bucket] = cell;
  }   
 

  cell->value = value;
}

//Get Value
template <typename ValueType>
ValueType MyMap<ValueType>::GetValue(string key){

  int bucket = hash(key);
  cellT *cell = FindCell(buckets[bucket], key);
  
  return cell->value;

}


/*
 * Private Functions
 */

// Find Cell
template <typename ValueType>
typename MyMap<ValueType>::cellT* MyMap<ValueType>::FindCell(cellT *cell, string key){
  while (cell != NULL) {
    if(cell->key == key) return cell;
    cellT *next = cell->link;
    cell = next;
  }
  return cell;
}


// Delete BucketChain
template <typename ValueType>
void MyMap<ValueType>::deleteBucketChain(cellT *cell){
  while (cell != NULL) {
    cellT *next = cell->link;
    delete cell;
    cell = next;
  }
}

template <typename ValueType>
int MyMap<ValueType>::hash(string s){
  const long multiplier = 10;
  unsigned long hashcode = 0;
  for(int i =0; i < s.length(); i++){
    hashcode = hashcode * multiplier + s[i];
  }
  return(hashcode % nBuckets);
}

