#ifndef _MY_MAP_H_
#define _MY_MAP_H_

/*
 * MyMap.h
 * Interface for a Hast Table Class
 * Built by me!
 */

#include <string>
//#include "genlib.h"


using namespace std;

template <typename ValueType>
class MyMap {

 public:
    //Constructor
  MyMap();
  //Destructor
  ~MyMap();
  
  

  void Add(string key, ValueType value);
  ValueType GetValue(string key);
  

 private:
  static const int nBuckets = 101;
  
  struct cellT{
    string key;
    ValueType value;
    cellT * link;  
  };

  cellT *buckets[nBuckets];

  void deleteBucketChain(cellT * cell);
  cellT *FindCell(cellT * cell, string key);
  int hash(string s);
  
};

#include "my_map.cc"
#endif
