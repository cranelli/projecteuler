/*
 * Christopher Anelli
 * Aug 30, 2015
 * Short piece of code to see how many paths there are from the lower left
 * corner of a 20X20 grid to the upper right corner, only moving right
 * or up.
 * 32 bit integer issues
 */

#include <iostream>
#include <string>
#include <map>

using namespace std;

const int x_end = 15;
const int y_end = 15;

map<string, long double> stored_paths;

long double CountPaths(int x, int y);

/*
 * (0,0) is the lower left corner.
 */
int main(){
  int x_start = 0;
  int y_start = 0;
  cout << sizeof(long double) << endl;
  int paths = CountPaths(x_start, y_start);
  cout << paths << endl;
}

long double CountPaths(int x, int y){
  string pos = to_string(x) + "," + to_string(y);
  if(x==x_end && y==y_end){
    stored_paths[pos]=1LL;
  }
  if(x > x_end || y > y_end){
    stored_paths[pos] = 0LL; 
  }
 
  map<string,long double>::iterator it;
  it = stored_paths.find(pos);
  if(it ==stored_paths.end()){ // not found
    stored_paths[pos] = CountPaths(x+1, y) + CountPaths(x, y+1);
  }
  return stored_paths[pos];
}     
