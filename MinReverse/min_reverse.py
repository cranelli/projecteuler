#!/usr/bin/env python3

# If one could only move elements to the end,
# find the minimum number of moves required to sort
# a given array.

# It is not necessary to know the order of moves. One can tell
# that if an element comes before an element less than it, it
# will eventually require a move.  And if one efficiently orders
# the elements less that it first, only one move is required.

# Iterating through the array, count the number of times an
# element is less than the minimum element.  If you reach the
# minimum element, increment to next smallest, and so on.

# To know the smallest, next-to-smallest, and overall order,
# the array is sorted beforehand.  Because of the sorting,
# the solution will take O(NlnN) time.

def min_reverse(arr):
    sorted_arr = sorted(arr)
    
    count = 0
    i, j = 0, 0
        
    # Iterate over array.  
    for i in range(0, len(arr)):
        if(arr[i] > sorted_arr[j]): count+=1
        if(arr[i] == sorted_arr[j]): j += 1
        i+=1
    return count
    
    
    
if __name__ == "__main__":
    print(min_reverse( [2,3,4,5,1]))
    
