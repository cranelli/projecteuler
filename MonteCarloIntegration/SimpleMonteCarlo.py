#
# Simple Code for Monte Carlo Integration
# Test Function f(x)=cos(x), integrated from 0 to 3pi/2
#

import random
import math

def SimpleMC():
    N=1000000
    N_pass=0
    for i in range(0, N):
        if i % 1000 ==0: print i
        x = random.random()*(3.0/2)*math.pi
        y = -1+2*random.random()

        val = f(x)
        if y > 0 and y < val:
            N_pass+=1
        if y < 0 and y > val:
            N_pass-=1

    integral = (N_pass/(N*1.0))*3*math.pi
    print "Integral:", integral

def f(x):
    return math.cos(x)
    

if __name__ == "__main__":
    SimpleMC()


