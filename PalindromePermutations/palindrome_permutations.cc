#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <set>


using namespace std;

map<char, int> CountCharFrequency(string str);
bool IsPalindrome(map<char, int> & char_freq_map);
vector<string> FindPermutations(map<char, int> & char_freq_map);
//void SubtractChar(char key, map<char, int> & char_freq, int n=1);
//void Permutations(vector<string> & list, string center, map<char, int> & char_freq);
void permute(set<string> & permutation_set, string str);
void permute(set<string> & permutation_set, string str, int i, int n);

int main(int argc, char* argv[]){
    if(argc != 2){ return 0;}
    
    string str = argv[1];
    vector<string> palindrome_list;
    map<char, int> char_freq_map;
    
    char_freq_map = CountCharFrequency(str);
    
    bool is_palindrome = IsPalindrome(char_freq_map);
    cout << "is_pallindrome equals " << is_palindrome << endl;
    
    if(is_palindrome){
        palindrome_list = FindPermutations(char_freq_map);
    
        for(auto palindrome : palindrome_list){
            cout << palindrome << " ";
        }
        cout << endl;
    }
    
    return 1;
}
//Converts string into a map of character frequency
map<char, int> CountCharFrequency(string str){
    map<char, int> freq_store;
    for(auto c : str){
        if(freq_store.count(c)==0) {
            freq_store[c] =1;
        } else {
            freq_store[c]+=1;
        }
    }
    return freq_store;
}

// Returns whether or not a given character frequency map
// corresponds to a pallindrome.  (No more than 1 odd frequency count)
bool IsPalindrome(map<char, int> & char_freq_map){
    int odd_frequency_count = 0;
    for(auto pair : char_freq_map){
        if(pair.second%2 != 0){ odd_frequency_count++;}
    }
    //cout << odd_frequency_count << endl;
    return odd_frequency_count <= 1;
}

// Wrapper for Permutations
// Finds the center character and the first half of the pallindrome
// Then gets all the permutations of the first half.
vector<string> FindPermutations(map<char, int> & char_freq){
    vector<string> palindrome_list;
    set<string> permutation_set; //Only interested in distinct combinations
    string str, center = "";
    map<char, int> char_freq_copy = char_freq;
    
    //If there is an odd character count, that character is center
    for(auto pair : char_freq){
        if(pair.second %2 != 0){
            center = pair.first;
            
            //SubtractChar(pair.first, char_freq_copy);
        }
        while(pair.second != 1){
            str += pair.first;
            pair.second /=2;
        }
    }
    permute(permutation_set, str);
    
    for(auto str: permutation_set){
        string rstr(str);
        reverse(rstr.begin(), rstr.end());
        palindrome_list.push_back(str + center + rstr );
    }
    return palindrome_list;
}
//Wrapper for permute
void permute(set<string> & permutations_set, string str){
    permute(permutations_set, str, 0, str.size());
}

void permute(set<string> & permutation_set, string str, int i, int n){
    if(i == n-1){
        permutation_set.insert(str);
        return;
    }
    //swap, recurse, backtrack
    for(int j = i; j < n; j++){
        swap(str[i], str[j]);
        permute(permutation_set, str, i+1, n);
        swap(str[i], str[j]);
    }
}
    
        

/*
// Subtracts 1 from character frequency, if count is 0, removes
// entry from the map.
void SubtractChar(char key, map<char, int> & char_freq, int n){
    if(char_freq.count(key)!=0){
        char_freq[key]-=n;
        if(char_freq[key]==0) {char_freq.erase(key);}
    }
}

//Recursive method for finding permutations of a string.
void Permutations(vector<string> & list, string center, map<char, int> & char_freq){
    if(char_freq.size()== 0){
        list.push_back(center);
    } else {
        string new_center;
        for(auto pair : char_freq){
            new_center = pair.first + center + pair.first;
            map<char, int> char_freq_copy = char_freq;
            SubtractChar(pair.first, char_freq_copy, 2);
            Permutations(list, new_center, char_freq_copy);
        }
    }
}
*/
