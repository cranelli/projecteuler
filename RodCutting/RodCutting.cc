/*
 * Example of Dynamic Programming
 * Find Optimal Positions to Cut a Rod
 */

#include <iostream>
#include <vector>
#include <map>

using namespace std;

map<int, int> PRICE_STORE;

int OptimalPrice(int n, vector<int> & prices);

int main(){

  int arr [] = {0, 1, 5, 8, 9, 10, 17, 17, 20, 24, 30};
  vector<int> prices(arr, arr + sizeof(arr)/sizeof(int));

  int n = 15;
  cout << OptimalPrice(n, prices) << endl;
  return 1;

}

/* 
 * Finds optimal price
 * solutions for sub-lenths are store in PRICE_STORE
 */
int OptimalPrice(int n, vector<int> & prices){
  if(n == 0) return 0;

  if(PRICE_STORE.count(n) == 0){
    int max_price = 0;
    // length starts at 1
    for(int length = 1; length < prices.size(); length++){
      if(n - length < 0) continue;
      int price = prices[length] + OptimalPrice(n - length, prices);
      if(price > max_price){
	max_price = price;
      }
    }
    PRICE_STORE[n] = max_price;
  }
  return PRICE_STORE[n];
}

