/*
 * Modified version of the QuickSort algorithm, for finding
 * the median value in an unsorted list of numbers.
 * Runs in O(N) time
 *
 * For simplicity can assume N is odd.
 */

#include <iostream>
#include <vector>

using namespace std;

void MedianSort(vector<int> & arr, int start, int stop);
int  Partition(vector<int> & arr, int start, int stop);

int main(){
  vector<int> sequence;
  int array[7] = {2, 8, 7, 1, 3, 5 ,6}; // List to Sort
  sequence.assign(array, array + 7);

  MedianSort(sequence, 0, sequence.size());
  
  // Print out sorted array
 
  cout << "Median: " << sequence[sequence.size()/2] << endl;;
 
}

/*
 *Partitions the array around a pivot point.
 * To the left all elems are <= pivot value.
 * To the right all elems are > pivot value.
 * Then recursively partitions each of the subarrays,
 * until list is completely sorted.
 */

void MedianSort(vector<int> & arr, int start, int stop){
  //cout << "working" << endl;
  if(stop - start > 1) { 
    int pivot_point = Partition(arr, start, stop);

    // Only sort the partition that contains the Median.
    
    if(pivot_point > arr.size()/2){
      MedianSort(arr, start, pivot_point);
    }
    if(pivot_point < arr.size()/2){

      MedianSort(arr, pivot_point +1, stop);
    }
    //MB pivot point is not sorted again
  }
}

/*
 * Partitions subarray into elms < and > pivot value.
 * Pivot value is the first element;s value.
 * Loop over subarray, with two indices keeping track of 
 * < and > partitions.  (i,j)
 * Place pivot between the two partitions, return index pos.
 */

int Partition(vector<int> & arr, int start, int stop){
   cout << "start " << start  << " stop " << stop << endl;
  int pivot = arr[start];
  cout << "pivot " << pivot << endl;
  
  
  int i = start+1;
  
  for(int j =start+1; j < stop; j++){
    cout << arr[j] << " ";
    if(arr[j] <= pivot){
      int swap = arr[i];
      arr[i] = arr[j];  
      arr[j] = swap;
      i++;
    }
  }
  cout << endl;
  
  // Place pivot between the two partitions
  arr[start] = arr[i-1];
  arr[i-1] = pivot;

  return i-1;
}
