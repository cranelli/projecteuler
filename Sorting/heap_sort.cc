/*
 * Heapsort Algorithm 
 * Each element in the array is a node
 * Sorts in O(nlgn) time.
 * Advantage, Sorting is done in place
 */

#include <iostream>
#include <vector>

using namespace std;


struct Heap{
  int * array;
  int length;
};


void HeapSort(Heap & heap);
void BuildHeap(Heap & heap);
void FloatDown(Heap & heap, int node);

int main(){
  
  const int n = 8;
  int arr[n] = {2, 8, 7, 1, 3, 5 ,6, 4}; // List to Sort

  Heap heap;
  heap.array = arr;
  heap.length = n;

  HeapSort(heap);

  for(int i=0; i < n; i++){
    cout << heap.array[i] << " ";
  }
  cout << endl;

}

/*
 * Heap is ordered as binary tree, with maximum element
 * at the root, array[0].
 */
void HeapSort(Heap & heap){
  BuildHeap(heap);
  

  int root = 0;
  int array_end = heap.length -1;

  for(int i = array_end; i > 0; i--){
    int max_heap = heap.array[root];
    //Swap max with element at i.
    heap.array[root] = heap.array[i];
    heap.array[i] = max_heap;
    heap.length--;
    FloatDown(heap, root);
  }
}


/*
 * Using FloatDown, from bottom up constructs a heap.
 * can start at heap size /2, since elements below have no
 * children and are already in order.
 */
void BuildHeap(Heap & heap){
  for(int i =heap.length/2 -1; i >= 0; i--){
    FloatDown(heap, i);
  }
}

/*
 * FloatDown algorithm maintain's the heap's structure.
 * Assuming elements below node-i are properly sorted.
 * Value of parent is > value of children
 */

void FloatDown(Heap & heap, int node){
  //if(node > heap.length /2 -1) return;

  int left = 2*node+1;
  int right = 2*node +2;
  int largest = node;
  int new_max;

  if(left < heap.length && heap.array[left] > heap.array[node]){
    largest = left;
  }

  if(right < heap.length && heap.array[right] > heap.array[largest]){
    largest = right;
  }

  if(largest != node){
    new_max = heap.array[largest];
    heap.array[largest] = heap.array[node];
    heap.array[node] = new_max;
    FloatDown(heap, largest);
  }
}

  
