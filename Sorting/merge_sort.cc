/*
 * Merge Sort function to sort an array.
 */

#include <iostream>
#include <vector>
#include <limits>

using namespace std;

//const int NUM_ENTRIES = 5;


void MergeSort(vector<int> & sequence, int start, int end);
void Merge(vector<int> & sequence, int start, int mid_point, int end);

int main(){
  vector<int> sequence;
  int array[5] = {2, 5, 7, 3, 1};
  sequence.assign(array, array +5);
  
  MergeSort(sequence, 0, sequence.size()-1);
  
  for(int i =0; i < sequence.size(); i++){
    cout << sequence[i] << " ";
  }
  cout << endl;
}

/*
 * Recursively Sorts the Sample, by sorting
 * samples of half the size.
 */
void MergeSort(vector<int> & sequence, int start, int end){
  if(start < end){
    int mid_point = (start + end)/2;
    MergeSort(sequence, start, mid_point);
    MergeSort(sequence, mid_point +1, end);
    Merge(sequence, start, mid_point, end);
  }
}

/*
 * Merges to sorted sub-arrays,
 * split at midpoint.
 */

void Merge(vector<int> & sequence, int start, int mid_point, int end){
  
  int num_left = mid_point - start +1;
  int num_right = end - mid_point;

  vector<int> left(sequence.begin() + start, sequence.begin() + mid_point +1);
  vector<int> right(sequence.begin() + mid_point +1, sequence.begin()+end +1);


  //  vector<int> left, right;
  //left.assign
  //right.assign(sequence.begin() + start + num_left, sequence.begin() + end +1);
  
  int infinity = numeric_limits<int>::max();
  left.push_back(infinity);
  right.push_back(infinity);
  

  int i=0;
  int j =0;

  int k;
  //cout << "End " << end << endl;
  for(k = start; k <= end; k++){
    if(left[i] < right[j]){
      sequence[k] = left[i];
      i++;
    }
    else {
      sequence[k] = right[j];
      j++;
    }
  }
	
  /*
  for(i= 0; i< left.size(); i++){
    cout << left[i] << endl;
  }
  
  cout << endl;

  for( j =0; j < right.size(); j++){
    cout << right[j] << endl;
  }
  
  cout << endl;

  for( k =0; k < sequence.size(); k++){
    cout << sequence[k] << endl;
  } 
  
  cout << endl;
  */
}
