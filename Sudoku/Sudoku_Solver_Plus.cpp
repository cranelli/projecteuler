/*
 * Sudoku Solver
 * Christopher Anelli
 * 8/18/2012
 */

#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

static const int size = 9;  //To have boxes, size should have an integer square root.


//void Generate_Board(int Grid[size][size]);
void Generate_Board(int Grid[size][size]);
void Display_Board(int Grid[size][size]);
void Setup_Board(int Grid[size][size]);
void Solve_Board(int Grid[size][size]);
bool Solve(int row, int col, int Grid[size][size]);
bool Is_Solution(int Grid[size][size]);
void Change_Entry(int Grid[size][size], int row, int col);
bool Row_Pass(int row, int val, int Grid[size][size]);
bool Col_Pass(int col, int val, int Grid[size][size]);
bool Box_Pass(int box, int val, int Grid[size][size]);
void Manual_Setup(int Grid[size][size]);
void File_Setup(int Grid[size][size]);
bool Is_Allowed(int val, int row, int col, int Grid[size][size]);
bool Solve_Next(int row, int col, int Grid[size][size]);
bool Outside_Grid(int row, int col);

int main(){
  int Grid2 [size][size];
  int Grid [size][size];
  Generate_Board(Grid);
  Setup_Board(Grid);
  cout << "Here is the starting board" << endl;
  Display_Board(Grid);
  Solve_Board(Grid);
  if(Is_Solution(Grid)) Display_Board(Grid); //For the case no solution exists, no board is displayed
  
}

/*
 * Fills the 2-D array of sudoky squares with zero values, and sets the fixed
 * parameter to false.
 */

void Generate_Board(int Grid[size][size]){
  for(int i = 0; i < size; i++){
    for(int j = 0; j < size; j++){
      Grid[i][j] = 0;
    }
  }
}

/*
 * Prints out the current values of the Sudoku Board
 */

void Display_Board(int Grid[size][size]){
  for(int i = 0; i < size; i++){
    for(int j = 0; j < size; j++){
      cout << Grid[i][j] <<" ";
    }
    cout << endl;
  }
}
/*
 * Setup the Board, having the user select to do it either manually or from a file.
 */
void Setup_Board(int Grid[size][size]){
  string file;
  cout << endl;
  cout <<"Please Setup the Sudoku Board" <<"(Size " << size << "x" << size << ")"<< endl;
  cout <<"Would you like to setup the board manually, or read it in from a file? (m/f): "; cin >> file;
  if(file[0]=='f'){
    File_Setup(Grid);
  }
  else{
    Manual_Setup(Grid);
  }
}

/*
 *The user is prompted for the name of the file.  
 *It is read in line by line.
 *Each character is taken as a unique entry, and is converted
 *to and int by subtracting '0'. 
 *The values are then placed in the grid, and set as fixed.  
 *Assumes that blank spaces will be marked with a 0.
 */
 
void File_Setup(int Grid[size][size]){
  ifstream setup_file;
  string setup_file_name;
  string line;
  //cout << "What is the name of the file: "; cin >> setup_file_name; //Remember to uncomment
  setup_file_name = "Sudoku_Board.txt";//Temporary for easier debugging remeber to remove
  setup_file.open(setup_file_name.c_str());
  for(int i =0; i<size; i++){
    getline(setup_file,line);
    for(int j =0; j<size; j++){
      Grid[i][j] = (line[j]-'0');
    }
  }
  setup_file.close();
}

/* 
 * User is prompted to manually setup the Sudoku Board.
 * Giving the row, col, and value;
 * The Fixed parameter is set to true.
 */
void Manual_Setup(int Grid[size][size]){
  int row_in, col_in, value_in;
  string more;
  cout <<"Put each of the given values into the appropriate row and column." << endl;  
  cout << "(With 1,1 being in the upper left.)" << endl;
  while(true){
    cout << "Row: "; cin >> row_in;
    cout << "Col: "; cin >> col_in;
    cout << "Value "; cin >> value_in;
    if(value_in >size || value_in < 1){
      cout << "The value must be an integer bewteen 1 and 9" << endl;
      cout << "Value "; cin >> value_in;
    }
    Grid[row_in-1][col_in-1] = value_in;
    cout << "Do you have more values to insert? y/n" << endl; cin >> more; cout << endl;
    if(more[0] == 'n') break;
  }
}
/*
 * Wrapper for Solve
 */

void Solve_Board(int Grid[size][size]){
  if(Solve(0,0,Grid)){
    cout << "Here is the Solved Sudoku Puzzle" << endl;
  }
  else{
    cout <<"No Solution Exists" << endl;
  }
}
/*
 * Uses Recursion to Solve the Sudoku Puzzle
 */

bool Solve(int row, int col, int Grid[size][size]){     
  if(Is_Solution(Grid)) return true;
  if(Outside_Grid(row, col)) return false;
  if(Grid[row][col]!=0){
    return(Solve_Next(row,col,Grid));
  }
  else {
    for(int val =1; val <=9; val++){
      Grid[row][col] =val;
      if(Is_Allowed(val,row,col,Grid)){
	if(Solve_Next(row,col,Grid)) return true;
      }
    }
    Grid[row][col] = 0;
    return false;
  }
}


/*
 *Moves to the next square in the row.  If it is the end of the row,
 *it goes to the begining of the next one.
 */
bool Solve_Next(int row, int col, int Grid[size][size]){
  if(col == size-1){
    return(Solve(row+1, 0, Grid));
  }
  else {
    return(Solve(row, col+1, Grid));
      }
}
/*
 *Checks to see if the current position is outside the Sudoku Grid.
 */
bool Outside_Grid(int row, int col){
  if(row >= size || col >= size){
    return true;
  }
  return false;
}

/*
 *Checks to  make sure that for the value place in position (row,col)
 *There is only one of that number in the corresponding row, column,
 *and box.
 */

bool Is_Allowed(int val, int row, int col, int Grid[size][size]){
  int sqrt_size = sqrt(size);
  int box = (row/sqrt_size)*sqrt_size + col/sqrt_size;
  if(!Row_Pass(row, val, Grid)) return false;
  if(!Col_Pass(col, val, Grid)) return false;
  if(!Box_Pass(box, val, Grid)) return false;
  return true;
}
     

/*
 * Checks to see if the current board layout is the correct solution.
 */

bool Is_Solution(int Grid[size][size]){
  for(int i =0; i< size; i++){
    for(int val =1; val<=size; val++){
      if(!Row_Pass(i,val,Grid)) return false;
      if(!Col_Pass(i,val,Grid)) return false;
      if(!Box_Pass(i,val,Grid)) return false;
    }
  }
  return true;
}

//Checks to make sure there is one of every number in each row

 bool Row_Pass(int row, int val, int Grid[size][size]){
   int count = 0;
   for(int col=0; col < size; col++){
     if(Grid[row][col]==val) count ++;
   }
   if(count != 1) return false;
   return true;
}
  
//Checks to make sure there is one of every number in each column

 bool Col_Pass(int col, int val, int Grid[size][size]){
   int count = 0;
   for(int row=0; row < size; row++){
     if(Grid[row][col]==val) count ++;
   }
   if(count != 1) return false;
   return true;
 }

//Checks to make sure there is one of every number in each box

bool Box_Pass(int box, int val, int Grid[size][size]){
  int sqrt_size = sqrt(size);
  int row = (box/sqrt_size)*sqrt_size; //Value can be different than box
  int col = (box%sqrt_size)*sqrt_size;  
  int count = 0;
  for(int a=0; a < sqrt_size; a++){
    for(int b=0; b < sqrt_size; b++){
      if(Grid[row+a][col+b]==val) count ++;
    }
  }
  if(count != 1) return false;
  return true;
}
