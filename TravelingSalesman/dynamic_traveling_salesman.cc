#include <vector>
#include <iostream>
#include <cmath>
#include <map>

/*
 *  Program to solve the traveling salesman problem using dynamic programming.
 *  Uses the Held-Karp algorithm.
 */

using namespace std;

vector< vector<double> > BuildDistanceGrid(vector< vector<double> > & locations, vector< vector<double> > & distances);
double CalcDistance(vector< vector<double> > & locations, int i, int j);
void FindRoute(vector< vector<double> > & distances);
double RecursiveFindRoute(int end, int mask, vector <vector<double> > & distances);


map< pair<int, int>, double> STORE; 

int main(){
  int num_cities;

  cin >> num_cities;
  vector< vector<double> > locations(num_cities, vector<double>(3) ) ;

  for(int i =0; i < num_cities; i++){
    // Loop over Label, x position, y position
     for(int j =0; j <3; j++){
       cin >> locations[i][j];
     }
  }
  
  vector< vector<double> > distances(num_cities, vector<double>(num_cities) );

  BuildDistanceGrid(locations, distances);
  FindRoute(distances);
   
  return 0;

}

/*
 * From the location grid, returns a grid of the distance of each city from 
 * the others.
 * d_ij distance from city i to city j
 */

vector< vector<double> > BuildDistanceGrid(vector< vector<double> > & locations, vector< vector<double> > & distances){

  int num_cities = distances.size();

  for(int i=0; i < num_cities; i++){
    for(int j =0; j < num_cities; j++){
      // Use Symetry of Grid
      if(j > i){
	continue;
      } else if(i == j){ 
	distances[i][j] = 0;
      } else {
	double d = CalcDistance(locations, i, j);
	distances[i][j] = d;
	distances[j][i] = d;
      }
    }
  }
  
  return distances;
}


double CalcDistance(vector< vector<double> > & locations, int i, int j){
  double x_i = locations[i][1];
  double y_i = locations[i][2];
  double x_j = locations[j][1];
  double y_j = locations[j][2];

  double distance = sqrt( pow(x_i - x_j,2) + pow(y_i - y_j, 2) );
  return distance;
}


void FindRoute(vector< vector<double> > & distances){
  int n = distances.size();

  int mask = (1 << n) -1;

  double min_path = RecursiveFindRoute(0, mask, distances);

  cout << min_path << endl;
  //return min_path;
}


/*
 * Use mask to identiy a subset.
 */
double RecursiveFindRoute(int end, int mask, vector<vector<double> > & distances){
  int n = distances.size();

  if(mask == 1) return distances[0][end];
  

  pair<int, int> key(end, mask);
  if(STORE.count(key) == 0){
    double min_distance = 10000;
    double distance;
    for(int i = 1; i < n; i++){
      if( (mask & (1 << i) ) > 0){ // Make Sure City is contained in Subset
	int new_mask = mask - (1 << i);
	distance = RecursiveFindRoute(i, new_mask, distances) + distances[i][end];
	if(distance < min_distance) min_distance = distance;
      }
    }
    STORE[key] = min_distance;
  }

  return STORE[key];
}
  
      
  
  
 
 
    

