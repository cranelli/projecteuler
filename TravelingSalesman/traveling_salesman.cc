#include <vector>
#include <iostream>
#include <cmath>

/*
 * Program to solve the traveling salesman problem 
 * Brute Force O(n!)
 */

using namespace std;

vector< vector<double> > BuildDistanceGrid(vector< vector<double> > & locations, vector< vector<double> > & distances);
double CalcDistance(vector< vector<double> > & locations, int i, int j);
void FindRoute(vector< vector<double> > & distances);
int MinRoute(vector< vector<int> > & list_of_permutations, vector< vector<double> > & distances);
double PathDistance(int city_index, vector<int> set, vector< vector<double> > & distance);
void ListPermutations(vector< vector<int> > & list_of_permutations, int num_cities);
void ListPermutations(vector<int> set, vector<int> permutation, vector< vector<int> > & list_of_permutations);

int main(){
  int num_cities;

  cin >> num_cities;
  vector< vector<double> > locations(num_cities, vector<double>(3) ) ;

  for(int i =0; i < num_cities; i++){
    // Loop over Label, x position, y position
     for(int j =0; j <3; j++){
       cin >> locations[i][j];
     }
  }
  
  vector< vector<double> > distances(num_cities, vector<double>(num_cities) );

  BuildDistanceGrid(locations, distances);
  FindRoute(distances);
   
  return 0;

}

/*
 * From the location grid, returns a grid of the distance of each city from 
 * the others.
 * d_ij distance from city i to city j
 */

vector< vector<double> > BuildDistanceGrid(vector< vector<double> > & locations, vector< vector<double> > & distances){

  int num_cities = distances.size();

  for(int i=0; i < num_cities; i++){
    for(int j =0; j < num_cities; j++){
      // Use Symetry of Grid
      if(j > i){
	continue;
      } else if(i == j){ 
	distances[i][j] = 0;
      } else {
	double d = CalcDistance(locations, i, j);
	distances[i][j] = d;
	distances[j][i] = d;
      }
    }
  }
  
  return distances;
}


double CalcDistance(vector< vector<double> > & locations, int i, int j){
  double x_i = locations[i][1];
  double y_i = locations[i][2];
  double x_j = locations[j][1];
  double y_j = locations[j][2];

  double distance = sqrt( pow(x_i - x_j,2) + pow(y_i - y_j, 2) );
  return distance;
}


void FindRoute(vector< vector<double> > & distances){
  int num_cities = distances.size();
  
  cout << "Distance Grid" << endl;
  for(int i =0; i < num_cities; i++){
    for(int j =0; j < num_cities; j++){
      printf("%2.2f ", distances[i][j]);
      //cout << distances[i][j] << " " ;
    }
    cout << endl;
  } 

  //Starting at 0 BruteForce Solution

  vector< vector<int> > list_of_permutations;
  ListPermutations(list_of_permutations, num_cities);

  int route_number = MinRoute(list_of_permutations, distances);


  cout << "Shortest Path" << endl;
  for(vector<int>::iterator it = list_of_permutations[route_number].begin(); it != list_of_permutations[route_number].end(); it++){
    cout << * it << " ";
  }
  cout << endl;
}

int MinRoute(vector< vector<int> > & list_of_permutations, vector< vector<double> > & distances){
  double min_distance = 10000;
  int min_route = 0;

  //for(vector<vector<int> >::iterator it = list_of_permutations.begin(); it != list_of_permutations.end(); it++){
  for(int route = 0; route < list_of_permutations.size(); route++){
    
    double distance = 0;
    //Loop through cities within path
    for(int i = 0; i < list_of_permutations[route].size(); i++){
      int j = (i + 1) % (list_of_permutations[route].size());  //At end loop back to the beginning
      //cout << i << " : " << j << endl;
      distance += distances[list_of_permutations[route][i]][list_of_permutations[route][j]];
      //cout << list_of_permutations[route][i];
    }
    //cout << endl;
    //cout << distance << endl;
    if(distance < min_distance){
      min_distance = distance;
      min_route = route;
    }

  }
  cout << "Minimum Distance " << min_distance << endl;
  return min_route;
}
    


// Wrapper for Function to List Permutations
void ListPermutations(vector< vector<int> > & list_of_permutations, int num_cities){
  vector<int> set;
  // All Permutation State (and end) at i
  for(int i =1; i < num_cities; i++){ 
    set.push_back(i);
  }

  vector<int> permutation(0);
  permutation.push_back(0);
  ListPermutations(set, permutation, list_of_permutations);
}

// Recursively lists all permutations that can be made from a given set.
void ListPermutations(vector<int> set, vector<int> permutation, vector< vector<int> > & list_of_permutations){
  if(set.size() == 0) list_of_permutations.push_back(permutation);

  for(vector<int>::iterator it = set.begin(); it!= set.end(); it++){
    int city_number = *it;
    permutation.push_back(city_number);
    set.erase(it);
    ListPermutations(set, permutation, list_of_permutations);
    set.insert(it, city_number);
    permutation.pop_back();
  }
}
    

