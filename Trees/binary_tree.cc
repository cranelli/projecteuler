/*
 * Code to Implement Binary Tree Class
 * Written by Christopher Anelli
 */

#include "binary_tree.h"
#include <iostream>

using namespace std;

//Public Functions

//Constructor

template <typename DataType>
BinaryTree<DataType>::BinaryTree(int (*func) (DataType, DataType)){
  compare = func;

  root = NULL;
}

template <typename DataType>
void BinaryTree<DataType>::Add(DataType elem){
  RecursiveAdd(root, elem);
}


//Returns Pointer to Location of Data Element in Tree, otherwise returns NULL.
template <typename DataType>
DataType * BinaryTree<DataType>::Find(DataType key){
  //cout << "key is " << key << endl;
  nodeT * found_node = RecursiveFind(root, key);
  
  if(found_node != NULL){
    return(& found_node->value);
  }
  return NULL;
}


//Private Functions
template <typename DataType>
typename BinaryTree<DataType>::nodeT * BinaryTree<DataType>::RecursiveFind(nodeT * node, DataType key){
  

  if(node == NULL) return NULL;

  //cout << "Node is " << node->value << endl;

  int sign = compare(key, node->value);
  
  if(sign == 0){
    return node;
  }

  if(sign < 0){
    return RecursiveFind(node->left, key);
  } else {
    return RecursiveFind(node->right, key);
  }
}


template <typename DataType>
void BinaryTree<DataType>::RecursiveAdd(nodeT * & node, DataType value){

  if(node == NULL){
    node = new nodeT;
    node->value = value;
    node->left = NULL;
    node->right = NULL;
    return;
  }

  int sign = compare(value, node->value);

  if(sign == 0) {
    node->value = value; 
  } else if (sign < 0){
    RecursiveAdd(node->left, value);
  } else {
    RecursiveAdd(node->right, value);
  }
  
}
