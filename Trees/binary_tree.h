/*
 * Header File For Binary Tree
 */

#ifndef _BINARY_TREE_H_
#define _BINARY_TREE_H_

#include <iostream>

using namespace std;

template<typename DataType>
class BinaryTree{
 public:
  BinaryTree (int (*func) (DataType, DataType));

  void Add(DataType elem);
  DataType * Find(DataType key);
  
 private:
  struct nodeT{
    DataType value;
    nodeT * left, * right;
  };

  nodeT * root;
  int (*compare) (DataType, DataType);


  //Helper Functions
  nodeT * RecursiveFind(nodeT * node, DataType key);
  void RecursiveAdd(nodeT * & node, DataType value);

};


#include "binary_tree.cc"

#endif
