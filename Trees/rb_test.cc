
/*
 * Short program to test the Tree Class
 */

#include "red_black_tree.h"
#include <iostream>
#include <vector>

using namespace std;

int compare(int a, int b){ 
  if(a == b) return 0;
  if(a > b){ 
    return 1;
  } else {
    return -1;
  }
}


int main(){
  //cout << "hello world" << endl;
  RedBlackTree<int> tree(compare);

  int array[] = {3, 4, 5};
  vector<int> values(array, array + sizeof(array)/sizeof(int) );

  
  for(int i = 0; i< values.size(); i++){
    cout << values[i] << endl;
    tree.Add(values[i]);
  }

  int search = 4;
  int * found = tree.Find(search);

  if(found == NULL){
    cout << "Value Not Found" << endl;
  } else {
    cout << *found << endl;
  }
  
};

