/*
 * Code to Implement Red Black Tree Class
 * Written by Christopher Anelli
 */

#include "red_black_tree.h"
#include <iostream>

using namespace std;

//Public Functions

//Constructor

template <typename DataType>
RedBlackTree<DataType>::RedBlackTree(int (*func) (DataType, DataType)){
  compare = func;
  root = NULL;

}

template <typename DataType>
void RedBlackTree<DataType>::Add(DataType elem){
  
  RecursiveAdd(root, NULL, elem); //Root has Null parent
}


//Returns Pointer to Location of Data Element in Tree, otherwise returns NULL.
template <typename DataType>
DataType * RedBlackTree<DataType>::Find(DataType key){
  //cout << "key is " << key << endl;
  nodeT * found_node = RecursiveFind(root, key);
  
  if(found_node != NULL){
    return(& found_node->value);
  }
  return NULL;
}


//Private Functions
template <typename DataType>
typename RedBlackTree<DataType>::nodeT * RedBlackTree<DataType>::RecursiveFind(nodeT * node, DataType key){
  
  if(node == NULL) return NULL;

  //cout << "Node is " << node->value;

  int sign = compare(key, node->value);
  
  if(sign == 0){
    return node;
  }

  if(sign < 0){
    return RecursiveFind(node->left, key);
  } else {
    return RecursiveFind(node->right, key);
  }
}


template <typename DataType>
void RedBlackTree<DataType>::RecursiveAdd(nodeT * & node, nodeT * parent, DataType value){
  /*
  if(node == NULL) { 
    cout << "Node Null" << endl;
  } else { 
  cout << "Node" << node->value << endl;
  }
  */
  if(node == NULL){
    node = new nodeT;
    node->color = RED;
    node->value = value;
    node->left = NULL;
    node->right = NULL;
    node->parent = parent;
    RedBlackBalance(node);
    return;
  }


  int sign = compare(value, node->value);

  if(sign == 0) {
    node->value = value;
  } else if (sign < 0){
    RecursiveAdd(node->left, parent, value);
  } else {
    RecursiveAdd(node->right, parent, value);
  }
  
}

template <typename DataType>
void RedBlackTree<DataType>::RedBlackBalance(nodeT * & node){

}


template <typename DataType>
void RedBlackTree<DataType>::RotateRight(nodeT * & node){
  nodeT * node_copy = node;
  node = node->left;
  node->parent = node_copy->parent;

  nodeT * subtree_copy = node->right;
  node_copy->left = subtree_copy;
  node_copy->parent = node;
  node->right = node_copy;
}

template <typename DataType>
void RedBlackTree<DataType>::RotateLeft(nodeT * & node){
  nodeT * node_copy = node;
  node = node->right;
  node->parent = node_copy->parent;

  nodeT * subtree_copy = node->left;
  node_copy->right = subtree_copy;
  node_copy->parent = node;
  node->left = node_copy;
}
