/*
 * Header File For Binary Tree
 */

#ifndef _REDBLACK_TREE_H_
#define _REDBLACK_TREE_H_

#include <iostream>

using namespace std;

template<typename DataType>
class RedBlackTree{
 public:
  RedBlackTree (int (*func) (DataType, DataType));

  void Add(DataType elem);
  DataType * Find(DataType key);
  
 private:
  enum colorT{RED, BLACK};

  struct nodeT{
    colorT color;
    DataType value;
    nodeT * left, * right, *parent;
  };

  nodeT * root;

  int (*compare) (DataType, DataType);


  //Helper Functions
  nodeT * RecursiveFind(nodeT * node, DataType key);
  void RecursiveAdd(nodeT * & node, nodeT * parent, DataType value);
  void RedBlackBalance(nodeT * & node);
  void RotateRight(nodeT * & node);
  void RotateLeft(nodeT * & node);

};


#include "red_black_tree.cc"

#endif
