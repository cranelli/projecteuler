/*
 * Problem to find the longest chain that can be made,
 * out of words in a given dictionary.
 * Condition for the chain is that when you remove a 
 * letter, the new word must still be a vaild word.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <unordered_map>

using namespace std;

const string DICTIONAIRY_LOCATION = "/usr/share/dict/words";

unordered_map<string, string> MakeDictionary(string loc);
string LongestChainWord(unordered_map<string, string> & dictionairy);
int BestChainLength(string word, unordered_map<string, string> & dict, 
	      int chain_length);
int PrintChain(string word, unordered_map<string, string> & dict,
		int chain_length);

int main(){
  unordered_map<string, string> dict = MakeDictionary(DICTIONAIRY_LOCATION);
  string longest_chain_word = LongestChainWord(dict);
  cout << "Word with the longest chain is: " << longest_chain_word << endl;;
  PrintChain(longest_chain_word, dict, 1);
  return 0;
} 

string LongestChainWord(unordered_map<string, string> & dictionary){
  int global_max_chain = 0;
  string longest_chain_word = "";
  string word;

  for(unordered_map<string, string>::iterator it = dictionary.begin();
      it != dictionary.end(); it++){
    word = it->first;  // word is the key and value
    //cout << word << endl;
    if(global_max_chain > word.size()) continue; // Cannot be longer chain
    int best_chain = BestChainLength(word, dictionary, 1);
    if(best_chain > global_max_chain) {
      global_max_chain = best_chain;
      longest_chain_word = word;
    }
  }
  return longest_chain_word;

}

/*
 * Returns Best Possible chain length,
 * for a given word 
 */
int BestChainLength(string word, unordered_map<string, string> & dict, 
	      int chain_length){
  int best_chain_length = chain_length;

  
  for(int i = 0; i < word.size(); i++){
    string new_word = word;
    new_word.erase(i, 1); // Only remove single character.

    // Check that word is in dictionairy.    
    if(dict.count(new_word) != 0){

      int new_chain_length = BestChainLength(new_word, dict, chain_length+1);      
      if(new_chain_length > best_chain_length) {
	best_chain_length = new_chain_length;
      }

    } 
  }
  
  return best_chain_length;
}

/*
 * Modified version of Best Chain, but now also prints the chain
 * Prints a word's chain
 */
int PrintChain(string word, unordered_map<string, string> & dict, int chain_length){
  cout << word << endl;
  int best_chain_length = chain_length;
  //string best_word = "";

  for(int i = 0; i < word.size(); i++){
    string new_word = word;
    new_word.erase(i, 1); // Only remove single character.                                                 

    // Check that word is in dictionairy.                                                                  
    if(dict.count(new_word) != 0){

      int new_chain_length = PrintChain(new_word, dict, chain_length+1);
      if(new_chain_length > best_chain_length) {
        best_chain_length = new_chain_length;
	//best_word = new_word;
      }

    }
  }
  return best_chain_length;

}

/*
 * From the dictionairy text file given,
 * stores the entries.  
 */


unordered_map<string, string> MakeDictionary(string loc){
  ifstream dic_file (loc.c_str());

  unordered_map<string, string> dict;

  
  string line;
  if(dic_file.is_open() ){
    while( getline(dic_file, line) ){
      // Makes sure words are lower-case
      transform(line.begin(), line.end(), line.begin(), ::tolower);
      
      dict[line] = line;
    }
  }
  
 
  return dict;
}


